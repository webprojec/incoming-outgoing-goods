<?php
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/remove_from_warehouse.php";
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/modify_warehouse_data.php";
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/save/out-goods/save_released_goods_data.php";

    function releasing_goods($connection, $entries, $price, $amount, $date, $batch_no_exists) {
        foreach ($entries as $entry) {
            if ($entry["amount"] < $amount) {
                $amount -= $entry["amount"];
                remove_from_warehouse($connection, $entry["id"]); 
                save_released_goods_data($connection, 
                    $entry["name"], 
                    $entry["price"], 
                    $price, 
                    $entry["amount"], 
                    $date, 
                    $entry["batch_no"],
                    $batch_no_exists
                );
            } else if ($entry["amount"] >= $amount) {
                if($entry["amount"] == $amount) {
                    remove_from_warehouse($connection, $entry["id"]);
                    save_released_goods_data(
                        $connection, 
                        $entry["name"], 
                        $entry["price"],
                        $price, 
                        $amount, 
                        $date, 
                        $entry["batch_no"],
                        $batch_no_exists
                    );
                } else {
                    modify_warehouse_data(
                        $connection, 
                        $entry, 
                        $price, 
                        $amount,
                        $date,
                        $batch_no_exists
                    );
                }
                break;
            }
        }
    }
?>