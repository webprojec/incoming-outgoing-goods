<?php
    function get_from_warehouse_by_name($connection, $name, $amount, $batch_no = false) {

        if (!$batch_no) {
            $get_amount = <<< EOF
                SELECT sum(amount) as total_amount FROM warehouse
                WHERE name = '$name'
                GROUP BY name
                HAVING sum(amount) >= $amount
            EOF;
        } else {
            $get_amount = <<< EOF
                SELECT sum(amount) as total_amount FROM warehouse
                WHERE name = '$name'
                AND batch_no = $batch_no
                GROUP BY name
                HAVING sum(amount) >= $amount
            EOF; 
        }

        $total_amount = pg_query($connection, $get_amount);
        if (!$total_amount) {
            echo pg_last_error($connection);
            echo "\tExiting...\n";
            die();
        }
        $total_amount = pg_fetch_all($total_amount);
        
        if (count($total_amount) == 0) {
            return [];
        } else {
            $total_available_amount = $total_amount[0]["total_amount"];
            if ($total_available_amount < $amount) {
                echo "\tAmount you requested is not available at the warehouse\n";
                return [];
            }
        }

        if (!$batch_no) {
            $get_all_entries = <<< EOF
                SELECT * FROM warehouse
                WHERE name = '$name'
                AND batch_no IS NULL
                ORDER BY date ASC
            EOF;
        } else {
            $get_all_entries = <<< EOF
                SELECT * FROM warehouse
                WHERE name = '$name'
                AND batch_no = $batch_no
                ORDER BY date ASC
            EOF;
        }

        $entries = pg_query($connection, $get_all_entries);
        if (!$entries) {
            echo pg_last_error($connection);
            echo "\tExiting...\n";
            die();
        }
        $entries = pg_fetch_all($entries);

        return $entries;
    }
?>