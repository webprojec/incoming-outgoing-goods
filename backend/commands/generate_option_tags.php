<?php
    function generate_option_tags($options) {
        $html_content = "";
        foreach($options as $option) {
            $option = $option["batch_no"];
            $html_content .= "<option value='$option'>$option</option>";
        }

        return $html_content;
    }
?>