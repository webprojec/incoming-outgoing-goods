<?php

    function modify_warehouse_data($connection, $entry, $price, $amount, $date, $batch_no_exists) {
        $id = $entry["id"];
        $modify = <<< EOF
            UPDATE warehouse
            SET amount = amount - $amount
            WHERE id = $id
        EOF;

        if (!pg_query($connection, $modify)) {
            echo pg_last_error($connection);
            echo "\tExiting...\n";
        }
        save_released_goods_data(
            $connection, 
            $entry["name"], 
            $entry["price"],
            $price, 
            $amount, 
            $date, 
            $entry["batch_no"],
            $batch_no_exists
        );
    }
?>