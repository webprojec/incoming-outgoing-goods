<?php
    function save_values_into_table(
        $connection, 
        $table,
        $name, 
        $price, 
        $amount, 
        $date, 
        $batch_no, 
        $batch_no_exists
        ) {
        if ($batch_no_exists) {
            $insert = <<< EOF
                INSERT INTO $table (name, price, amount, date, batch_no)
                VALUES (
                    '$name',
                    $price,
                    $amount,
                    '$date' :: date,
                    $batch_no
                )
            EOF;
        } else {
            $insert = <<< EOF
                INSERT INTO $table (name, price, amount, date)
                VALUES (
                    '$name',
                    $price,
                    $amount,
                    '$date' :: date
                )
            EOF;
        }
        

        if (!pg_query($connection, $insert)) {
            echo pg_last_error($connection);
            echo "\tExiting...\n";
            die();
        }
    }
?>