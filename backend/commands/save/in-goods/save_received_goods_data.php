<?php
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/save/in-goods/save_values_into_table.php";

    function save_received_goods_data(
        $connection, 
        $name, 
        $price, 
        $amount, 
        $date, 
        $batch_no, 
        $batch_no_exists
    ) {
        $without_batch_values = <<< EOF
            '$name',
            $price,
            $amount,
            '$date' :: date
        EOF;
        $with_batch_values = <<< EOF
            '$name',
            $price,
            $amount,
            '$date' :: date,
            $batch_no
        EOF;
        save_values_into_table(
            $connection, 
            "in_goods",
            $name, 
            $price, 
            $amount, 
            $date, 
            $batch_no,   
            $batch_no_exists
        );
        save_values_into_table(
            $connection, 
            "warehouse",
            $name, 
            $price, 
            $amount, 
            $date, 
            $batch_no,   
            $batch_no_exists
        );
        
    }
?>