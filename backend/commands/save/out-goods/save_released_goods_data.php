<?php
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/calculate/calculate_profit.php";
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/save/out-goods/save_to_out_goods_table.php";
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/save/out-goods/save_to_revenue_table.php";

    function save_released_goods_data($connection, $name, $bought_price, $sold_price, $amount, $date, $batch_no, $batch_no_exists) {
        save_to_out_goods_table(
            $connection, 
            $name, 
            $sold_price, 
            $amount, 
            $date, 
            $batch_no, 
            $batch_no_exists
        );
        $profit = calculate_profit($bought_price, $sold_price, $amount);
        $time = date("H:i:s");
        save_to_revenue_table(
            $connection, 
            $name, 
            $bought_price,
            $sold_price, 
            $amount, 
            $date, 
            $time,
            $batch_no, 
            $batch_no_exists,
            $profit
        );
        
        
    }
?>