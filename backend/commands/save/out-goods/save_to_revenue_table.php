<?php
    function save_to_revenue_table(
        $connection, 
        $name, 
        $bought_price,
        $sold_price, 
        $amount, 
        $date, 
        $time,
        $batch_no, 
        $batch_no_exists,
        $profit
    ) {
        if($batch_no_exists) {
            $insert = <<< EOF
                INSERT INTO revenues (
                    name, 
                    bought_price,
                    sold_price,
                    amount,
                    date,
                    time,
                    batch_no,
                    profit
                )
                VALUES (
                    '$name',
                    $bought_price,
                    $sold_price,
                    $amount,
                    '$date' :: date,
                    '$time' :: time,
                    $batch_no,
                    $profit
                )
            EOF;
        } else {
            $insert = <<< EOF
                INSERT INTO revenues (
                    name, 
                    bought_price,
                    sold_price,
                    amount,
                    date,
                    time,
                    profit
                )
                VALUES (
                    '$name',
                    $bought_price,
                    $sold_price,
                    $amount,
                    '$date' :: date,
                    '$time' :: time,
                    $profit
                )
            EOF;
        }

        if (!pg_query($connection, $insert)) {
            echo pg_last_error($connection);
            echo "\tExiting...\n";
        }
    }
?>