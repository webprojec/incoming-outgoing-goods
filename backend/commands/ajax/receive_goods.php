<?php
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/connect_to_db.php";
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/save/in-goods/save_received_goods_data.php";

    connect_to_db();
    $connection = get_connection();

    $incoming = $_POST;
    $name = $incoming["name"];
    $price = floatval($incoming["price"]);
    $amount = intval($incoming["amount"]);
    $date = $incoming["date"];
    $batch_no = $incoming["batchNo"];
    $batch_no_exists = $batch_no == '' || $batch_no === 'true' ? false : true;
    
    save_received_goods_data(
        $connection, 
        $name, 
        $price, 
        $amount, 
        $date, 
        $batch_no, 
        $batch_no_exists
    );

    pg_close($connection);
?>