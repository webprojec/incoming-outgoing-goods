<?php
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/connect_to_db.php";
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/get_from_warehouse_by_name.php";
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/releasing_goods.php";

    connect_to_db();
    $connection = get_connection();

    $incoming = $_POST;
    $name = $incoming["name"];
    $price = floatval($incoming["price"]);
    $amount = intval($incoming["amount"]);
    $date = $incoming["date"];
    $batch_no = $incoming["batchNo"];
    $batch_no_exists = $batch_no == '' || $batch_no === 'true' ? false : true;
    $errors = "";
    $error_exists = false;
    if (!$batch_no_exists) {
        $entries = get_from_warehouse_by_name($connection, $name, $amount);
        if (count($entries) == 0) {
            $errors .= "\n\tНет такого товара или количество меньше запрошенного\n";
            $error_exists = true;
        } else {
            releasing_goods($connection, $entries, $price, $amount, $date, $batch_no_exists);
        }
    } else {
        $entries = get_from_warehouse_by_name($connection, $name, $amount, $batch_no);
        if (count($entries) == 0) {
            $errors .= "\n\tНет такого товара или количество меньше запрошенного\n";
            $error_exists = true;
        } else {
            releasing_goods($connection, $entries, $price, $amount, $date, $batch_no_exists);
        }
    }

    pg_close($connection);
    echo json_encode(["error_exists" => true, "error_body" => $errors]);
?>