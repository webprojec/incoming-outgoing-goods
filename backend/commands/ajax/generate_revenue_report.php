<?php
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/connect_to_db.php";

    $from = $_POST["from"];
    $till = $_POST["till"];
    connect_to_db();
    $connection = get_connection();

    $calculate_total_profit = <<< EOF
        SELECT sum(profit) FROM revenues 
        WHERE date >= '$from' :: date 
        AND date < '$till' :: date
    EOF;

    $total_profit = pg_query($connection, $calculate_total_profit);

    if (!$total_profit) {
        echo pg_last_error($connection);
        echo "\t Exiting...\n";
        die();
    }

    $total_profit = pg_fetch_all($total_profit);
    $total_profit = $total_profit[0];
    $total_profit = $total_profit["sum"];

    $get_all_rows_for_period = <<< EOF
        SELECT date, time, name, sold_price, bought_price, amount, profit
        FROM (
            SELECT * FROM revenues
            ORDER BY date ASC
        ) AS ordered_revenue_table
        WHERE date >= '$from' :: date 
        AND date < '$till' :: date
    EOF;

    $ordered_rows = pg_query($connection, $get_all_rows_for_period);

    if (!$ordered_rows) {
        echo pg_last_error($connection);
        echo "\t Exiting...\n";
        die();
    }

    $ordered_rows = pg_fetch_all($ordered_rows);

    $table = urlencode($table);
    pg_close($connection);
    echo json_encode(array("ordered_rows" => $ordered_rows, "total_profit" => $total_profit));
?>