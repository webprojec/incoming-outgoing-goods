<?php
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/calculate/calculate_batch_no_selector.php";
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/generate_option_tags.php";
    
    $name = $_POST["name"];
    $batch_nums = calculate_batch_no_selector($name);
    $option_tags = generate_option_tags($batch_nums);
    echo json_encode($option_tags, JSON_UNESCAPED_SLASHES);              
?>