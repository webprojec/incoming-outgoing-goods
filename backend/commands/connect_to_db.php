<?php
    require $_SERVER["DOCUMENT_ROOT"]."/backend/env/database.php";
    
    $connection = null;

    function connect_to_db() {
        $host = HOST;
        $port = PORT;
        $dbname = DBNAME;
        $dbuser = DBUSER;
        $dbpassword = DBPASSWORD;

        $conn = pg_connect( "host = $host port = $port dbname = $dbname user = $dbuser password = $dbpassword" );
        
        if (!$conn) {
            echo "\n\tError: Unable to open database $dbname.\n";
            echo "\tExiting...\n";
            die();
        } 

        global $connection;
        $connection = $conn;

        // in_goods для приходов
        $create_table = <<< EOF
            CREATE TABLE IF NOT EXISTS in_goods (
                id SERIAL PRIMARY KEY,
                name VARCHAR(100),
                price REAL,
                amount INTEGER,
                date DATE,
                batch_no INTEGER
            )
        EOF;

        if (!pg_query($connection, $create_table)) {
            echo pg_last_error($connection);
            echo "\tExiting...\n";
            die();
        }

        // out_goods для расходов
        $create_table = <<< EOF
            CREATE TABLE IF NOT EXISTS out_goods (
                id SERIAL PRIMARY KEY,
                name VARCHAR(100),
                price REAL,
                amount INTEGER,
                date DATE,
                batch_no INTEGER
            )
        EOF;

        if (!pg_query($connection, $create_table)) {
            echo pg_last_error($connection);
            echo "\tExiting...\n";
            die();
        }

        // warehouse товары на складе
        $create_table = <<< EOF
            CREATE TABLE IF NOT EXISTS warehouse (
                id SERIAL PRIMARY KEY,
                name VARCHAR(100),
                price REAL,
                amount INTEGER,
                date DATE,
                batch_no INTEGER
            )
        EOF;

        if (!pg_query($connection, $create_table)) {
            echo pg_last_error($connection);
            echo "\tExiting...\n";
            die();
        }

        $create_table = <<< EOF
            CREATE TABLE IF NOT EXISTS revenues (
                id SERIAL PRIMARY KEY,
                name VARCHAR(100),
                bought_price REAL,
                sold_price REAL,
                amount INTEGER,
                date DATE,
                time TIME,
                batch_no INTEGER,
                profit REAL
            )
        EOF;

        if (!pg_query($connection, $create_table)) {
            echo pg_last_error($connection);
            echo "\tExiting...\n";
            die();
        }
    }

    function get_connection() {
        global $connection;
        return $connection;
    }

?>