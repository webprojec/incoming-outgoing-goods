<?php
    require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/connect_to_db.php";

    function calculate_batch_no_selector($name) {
        connect_to_db();
        $connection = get_connection();
        $options = "";

        $get_all_batch_nums = <<< EOF
            SELECT DISTINCT batch_no FROM warehouse
            WHERE name = '$name'
            AND batch_no IS NOT NULL
        EOF;

        $all_batch_nums = pg_query($connection, $get_all_batch_nums);

        if(!$all_batch_nums) {
            echo pg_last_error($connection);
            echo "\tExiting...\n";
            die();
        }

        $all_batch_nums = pg_fetch_all($all_batch_nums);
        array_unshift($all_batch_nums, ["batch_no" => ""]);
        return $all_batch_nums;
    }
?>