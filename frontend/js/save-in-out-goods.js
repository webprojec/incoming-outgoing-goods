function saveInOutGoods(type) {
    var requestUrl = type == "in" ? "../../backend/commands/ajax/receive_goods.php": 
                                    "../../backend/commands/ajax/release_goods.php";
    var name = $("#name").val();
    var price = $("#price").val();
    var amount = $("#amount").val();
    var date = $("#date").val();
    date = date !== "" ? date : new Date().toISOString().slice(0, 10);
    var batchNo = $("#batch-no").val();

    var validationResponse = validateUserInput(name, price, amount, batchNo);
    name = validationResponse[0];
    price = validationResponse[1];
    amount = validationResponse[2];
    batchNo = validationResponse[3];
    if(!validationResponse) {
        console.log("Fields filled incorrectly!");
    } else {
        console.log(validationResponse, date);
        $.ajax({
            url: requestUrl,
            type: "POST",
            dataType: "json",
            data: {
              name: name,
              price: price,
              amount: amount,
              date: date,
              batchNo: batchNo
            }
        }).done(function(response){
            console.log(typeof response.error_exists);
            if (response.error_exists) {
                $("#error-message").html(response.error_body);
                $("#error-message").css("display", "block");
            }
        });
    }

    
  
}