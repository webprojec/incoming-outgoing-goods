function validateUserInput(name, price, amount, batchNo) {
    name = validateName(name);
    price = validatePrice(price);
    amount = validateAmount(amount);
    batchNo = validateBatchNo(batchNo);
    console.log(name, price, amount, batchNo);
    return name && price && amount && batchNo ? [name, price, amount, batchNo] : false;
}

function validateName(name) {
    const nameRegex = /[a-zA-Z0-9.,]+$/gm;
    if (nameRegex.test(name)) {
        return name;
    } else {
        $("#error-name-format").show();
        return false;
    }
}

function validatePrice(price) {
    const priceRegex = /^([0-9]+([.][0-9]*)?|[.][0-9]+)$/gm;
    if (priceRegex.test(price)) {
        return price;
    } else {
        $("#error-price-format").show();
        return false;
    }
}

function validateAmount(amount) {
    const amountRegex = /^\d+$/gm;
    if (amountRegex.test(amount)) {
        return amount;
    } else {
        $("#error-amount-format").show();
        return false;
    }
}

function validateBatchNo(batchNo) {
    const batchNoRegex = /^\d+$/gm;
    if (batchNoRegex.test(batchNo)) {
        return batchNo;
    } else if(batchNo == ""){
        return true;
    }
}