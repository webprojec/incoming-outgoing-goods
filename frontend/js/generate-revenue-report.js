function generateRevenueReport() {
    var from = $("#from-date").val();
    var till = $("#till-date").val();

    if (validatePickedDates(from, till)) {
        // valid
        $("#message-container").css("display", "none");

        $.ajax({
            url: "../../backend/commands/ajax/generate_revenue_report.php",
            type: "POST",
            dataType: "json",
            data: {
                from: from,
                till: till
            }
        }).done(function generateTable(response) {
            var orderedRows = response["ordered_rows"];
            var totalProfit = response["total_profit"];

            var table, thead, tbody, tfoot, tr;

            thead = "<thead>";
            thead = thead + "<th>Дата</th>";
            thead = thead + "<th>Время</th>";
            thead = thead + "<th>Товар</th>";
            thead = thead + "<th>Выручка</th>";
            thead = thead + "<th>Себестоимость</th>";
            thead = thead + "<th>Количество</th>";
            thead = thead + "<th>Прибыль</th>";
            thead += "</thead>";

            tbody = "<tbody>";
            orderedRows.forEach(row => {
                tr = "";
                tr += "<tr>";
                tr += "<td>" + row["date"] + "</td>";
                tr += "<td>" + row["time"] + "</td>";
                tr += "<td>" + row["name"] + "</td>";
                tr += "<td>" + row["sold_price"] + "</td>";
                tr += "<td>" + row["bought_price"] + "</td>";
                tr += "<td>" + row["amount"] + "</td>";
                tr += "<td>" + row["profit"] + "</td>";
                tr += "</tr>"
                tbody += tr;
            });
            tbody += "</tbody>";

            tfoot = "<tfoot>";
            tfoot = tfoot + "<th colspan='6'>Итого</th>";
            tfoot = tfoot + "<th>" + totalProfit + "</th>";
            tfoot += "</tfoot>";

            table = "<table class='styled-table'>";
            table += thead;
            table += tbody;
            table += tfoot;
            table += "</table>";
            console.log(table);

            $("#generated-table").html(table);
            $("#submit-btn").css("display", "none");
        })
    } else {
        // non-valid
        $("#message-container").css("display", "flex");

    }
}