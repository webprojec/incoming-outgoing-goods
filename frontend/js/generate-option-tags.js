function generateOptionTags() {
    var documentTitle = document.title;
        var outgoing = documentTitle.includes("Продажа");
        if (outgoing) {
            $("#name").on("change", function(event) {
                var name = event.target["value"];
                $.ajax({
                    url: "../../backend/commands/ajax/generate_option_tags_ajax.php",
                    type: "POST",
                    data: {
                        name: name
                    } 
                }).done(function(response) {
                    $("#batch-no").html(response);
                })
            });
        }
}