<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="../styles/common.css"/>
    <link rel="stylesheet" type="text/css" href="../styles/revenue.css"/>
    <link rel="stylesheet" type="text/css" href="../styles/errors.css"/>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="../js/go-back-to-main.js"></script>
    <script src="../js/validate-picked-dates.js"></script>
    <script src="../js/generate-revenue-report.js"></script>
    <title>прибыль</title>
</head>
<body>
    <div class="main">
        <h1>Выберите период</h1>
        <div id="message-container">
            <div class="error-container">
                <h3>Даты выбраны некорректно. Начальная дата должна быть раньше чем конечная</h3>
            </div>
        </div>
        
        <div class="date-input-container">
            <div class="date-picker-wrapper">
                <label for="from-date">От :</label>
                <input id="from-date" type="date">
                <label for="till-date">До :</label>
                <input id="till-date" type="date">
            </div>
        </div>

        <div class="table-container" id="generated-table"></div>
        <div class="buttons-container">
            <div class="button-wrapper">
                <div onclick="goBackToMain()">
                    <button>Назад</button>
                </div>
                <div class="submit-button" id="submit-btn" onclick="generateRevenueReport()">
                    <button>Посчитать</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>