<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="../styles/common.css"/>
    <link rel="stylesheet" type="text/css" href="../styles/in-out.css"/>
    <link rel="stylesheet" type="text/css" href="../styles/errors.css"/>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="../js/go-back-to-main.js"></script>
    <script src="../js/generate-option-tags.js"></script>
    <script src="../js/save-in-out-goods.js"></script>
    <script src="../js/validate-user-inputs.js"></script>
    <?php 
        require $_SERVER["DOCUMENT_ROOT"]."/backend/commands/generate_option_tags.php";

        $request_type = $_POST["request-type"] == "in";
        $title = $request_type ? "Приход" : "Продажа";
        $price_label = $request_type ? "Приходная цена" : "Цена продажи";
        $date_label = $request_type ? "прихода" : "продажи";
        $submit_function = $request_type ? "saveInOutGoods('in')" : "saveInOutGoods('out')";
        
        if ($request_type) {
            $batch_no_input_element = <<< EOF
                <input type="text" id="batch-no">
            EOF;
        } else {
            $batch_no_input_element = <<< EOF
                <select id="batch-no" style="min-width: 140px;">
                    <option value=""></option>
                </select>
            EOF;
        }
    ?> 
    <title id="main-title"><?php echo $title;?> товара</title>
</head>
<body>
    <div class="main">
        <h1><?php echo $title;?> товара</h1>
        <h3 id="error-message"></h3>
        <div class="incoming-goods">
            <div class="error-container">
                <div id="error-name-format">
                    Только буквы и цифры
                </div>
                <div id="error-name-mandatory">
                    Обязательное поле
                </div>
            </div>
            <div class="input-container">
                <label for="name">Название продукта :</label>
                <input type="text" id="name">
            </div>

            <div class="error-container">
                <div id="error-price-format">
                    Целое число или число с дробной частью
                </div>
                <div id="error-price-mandatory">
                    Обязательное поле
                </div>
            </div>
            <div class="input-container">
                <label for="price"><?php echo $price_label;?> :</label>
                <input type="text" id="price">
            </div>

            <div class="error-container">
                <div id="error-amount-format">
                    Только целое число
                </div>
                <div id="error-amount-mandatory">
                    Обязательное поле
                </div>
            </div>
            <div class="input-container">
                <label for="amount">Количество :</label>
                <input type="text" id="amount">
            </div>

            <div class="error-container">
                <div id="error-message-specific">
                    Обязательное поле
                </div>
            </div>
            <div class="input-container">
                <label for="date">Дата <?php echo $date_label;?> :</label>
                <input type="date" id="date" style="min-width: 132px">
            </div>

            <div class="input-container" id="batch-no-container">
                <label for="batch-no">Номер партии :</label>
                <?php echo $batch_no_input_element; ?>
            </div>

            <div class="button-wrapper">
                <div onclick="goBackToMain()">
                    <button>Назад</button>
                </div>
                <div class="submit-button" onclick="<?php echo $submit_function;?>">
                    <button>Сохранить</button>
                </div>
            </div>
            
        </div>
    </div>
    <script>
        $("#date").val(new Date().toISOString().slice(0, 10));
        generateOptionTags();
        //outgoing = outgoing === 'false' ? true : false;
    </script>
</body>
</html>