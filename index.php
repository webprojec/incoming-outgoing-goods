<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" type="text/css" href="frontend/styles/common.css"/>
    <link rel="stylesheet" type="text/css" href="frontend/styles/index.css"/>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>sales doctor</title>
  </head>
  <body>
    <div class="center-welcome">
        <h1>Система для учета прихода и расхода товара</h1>
        <div>
            <form class="" action="frontend/request-forms/in-out.php" method="post">
                <input name="request-type" type="text" value="in" hidden>
                <button type="submit" name="button">Приход</button>
            </form>
            <form class="" action="frontend/request-forms/in-out.php" method="post">
                <input name="request-type" type="text" value="out" hidden>
                <button type="submit" name="button">Продажа</button>
            </form>
            <form class="" action="frontend/request-forms/revenue.php" method="post">
                <button type="submit" name="button">Прибыль</button>
            </form>
        </div>
    </div>
    <?php

    ?>
  </body>
</html>
